cd C:/projects/gtk-test
$USERPROFILE/.cargo/bin/cargo build --release
mkdir -p ./app/bin
mkdir -p ./app/share/icons
cp ./target/release/gtk-test.exe ./app/bin
ldd ./app/bin/gtk-test.exe | grep '\/mingw.*\.dll' -o | xargs -I{} cp "{}" ./app/bin
cp -r C:/msys64/mingw64/share/icons ./app/share # mingw64
zip -r app.zip ./app
appveyor PushArtifact ./app.zip
